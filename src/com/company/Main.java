package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        Scanner scannerName = new Scanner(System.in);

        System.out.println("First name?");
        firstName = scannerName.nextLine();

        System.out.println("Last name?");
        lastName = scannerName.nextLine();

        System.out.println("First Subject Grade:");
        firstSubject = scannerName.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject = scannerName.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject = scannerName.nextDouble();

        double ave = (firstSubject + secondSubject + thirdSubject)/3;

        int aveInt = (int)ave;

        System.out.println("Good Day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + aveInt);








    }
}
